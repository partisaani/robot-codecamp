*** Settings ***
Documentation     A test suite with a single test for testing the search functionality.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file. It makes sure that the search tool
...               works as intended

Resource          finnkino-resource.robot
Test Teardown     Close Browser

*** Test Cases ***
Search Movie
    Given Browser Is Opened To Finnkino Main Page
    And Search Is Opened
    Then Search Works As Intended