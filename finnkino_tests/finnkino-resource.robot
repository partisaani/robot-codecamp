*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library

*** Variables ***
${SERVER}           www.finnkino.fi
${BROWSER}          Firefox
${MAIN_PAGE_URL}    https://${SERVER}
${LOGIN_CLICKER}    document.querySelector('.popover-link').click()
${LOCATOR_LOGIN_BUTTON}     xpath=//div[contains(string(), "Kirjaudu sisään")]
${LOCATOR_SUBMIT_BUTTON}    xpath=//*[@class="popover-login-container"]/div/button
${LOCATOR_POPUP_FRAME}      xpath=//div/div/iframe[1]
${LOCATOR_MOVIE}            xpath=(//a[@class='btn btn-primary btn-lg hidden-xs'])[1]

*** Keywords ***
Open Browser To Finnkino Main Page
    Open Browser                        ${MAIN_PAGE_URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed                  0
    Main Page Should Be Open

Main Page Should Be Open
    Popup Should Be closed
    Title Should Be                     Finnkino - Etusivu
    Location Should Be                  ${MAIN_PAGE_URL}/
    Cookies Should Have Been Accepted

Popup Should Be Closed
    Run Keyword And Ignore Error        Close Popup Ad

Cookies Should Have Been Accepted
    Run Keyword And Ignore Error        Accept Cookies

Close Popup Ad
    Wait Until Page Contains Element    ${LOCATOR_POPUP_FRAME}
    ${AD_PRESENT} =                     Run Keyword And Return Status    Select Frame    ${LOCATOR_POPUP_FRAME}
    ${CLOSE_BUTTON_PRESENT} =           Run Keyword And Return Status    Wait Until Element IS Visible    close-button
    Run Keyword If    ${CLOSE_BUTTON_PRESENT}    Click Element    close-button
    Run Keyword If    ${AD_PRESENT} 	Unselect Frame

Open Movies Page
    Click Element                       xpath=//a[contains(string(), "Elokuvat")]
    Movies Page Should Be Open
    Popup Should Be Closed

Movies Page Should Be Open
    Title Should Be                     Finnkino - Ohjelmistossa nyt
    Location Should Be                  ${MAIN_PAGE_URL}/elokuvat/ohjelmistossa

Open Login Window
    Wait Until Element Is Visible       ${LOCATOR_LOGIN_BUTTON}
    Execute Javascript                  ${LOGIN_CLICKER}

Input Username
    Input Text      userName    a1970708@nwytg.com

Input Password
    Input Text      password    codecamp

Submit Credentials
    Click Button                        xpath=//*[@class="form-group"]/button

Choose Movie
    Wait Until Element Is Visible       ${LOCATOR_MOVIE}
    Click Element                       ${LOCATOR_MOVIE}

Change date
    Click Element                       xpath=//select[@name='dt']
    Click Element                       xpath=//option[@value='06.03.2018']

Pick A Ticket
    Click Element                       xpath=//select[@name='ShowPrices[0].Amount']
    Click Element                       xpath=//option[@value='1']
    Click Button                        xpath=//div[@class='col-sm-4 text-justify-md']/button

Confirm seat
    Wait Until Element Is Visible       xpath=//button[@class='btn btn-primary pull-right-sm']
    Click Button                        xpath=//button[@class='btn btn-primary pull-right-sm']

Login To Finnkino
    Open Browser To Finnkino Main Page
    Accept Cookies
    Open Login Window
    Input Username
    Input Password
    Submit Credentials
    Popup Should Be Closed

Accept Cookies
    Click Button                        xpath=//div[@id='Cookie-Popup']/div/button

Open Search
    Click Element    xpath=//div[@class='global-search-container']

Search
    Input Text      query    Varasto 2
    Click Button    xpath=//div[@class='input-group-btn']/button

#################################
#  Gherkin keywords start here  #
#################################

Browser Is Opened To Finnkino Main Page
    Open Browser To Finnkino Main Page

Movies Page Is Open
    Open Movies Page
    Movies Page Should Be Open

Synopses Should Not Be Empty
    @{SYNOPSES} =    Get Webelements    xpath=//p[contains(@class, 'short-synopsis')]
    :FOR    ${SYNOPSIS}    IN    @{SYNOPSES}
    \   ${SYNOPSIS_TEXT} =   Get Text     ${SYNOPSIS}
    \   Should Not Be Empty     ${SYNOPSIS_TEXT}

Titles Should Not Be Empty
    @{TITLES} =    Get Webelements    //h2[contains(@class, "list-item-desc-title")]/a[contains(@href, "/title/")]
    :FOR    ${TITLE}    IN    @{TITLES}
    \   ${TITLE_TEXT} =   Get Text     ${TITLE}
    \   Should Not Be Empty     ${TITLE_TEXT}

Search Is Opened
    Open Search

Popup Is Closed
    Popup Should Be Closed

Search Works As Intended
    Search
    
Date Should Be Changed
    Change date

Movie Can Be Chosen
    Choose Movie
    Choose Movie

Logging In To Finnkino Should Be Possible
    Login To Finnkino

Ticket Can Be Picked
    Pick A Ticket

Seat Can Be Confirmed
    Confirm seat


