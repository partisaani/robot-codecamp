*** Settings ***
Documentation     A test suite with a single test for adding tickets to shopping cart.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file. It makes sure that the user is able to
...               add tickets to their shopping cart.

Resource          finnkino-resource.robot

Test Setup        Login To Finnkino
Test Teardown     Close Browser

*** Test Cases ***
Add Ticket To Shopping Cart
    Given Popup Is Closed
    And Movies Page Is Open
    Then Date Should Be Changed
    And Movie Can Be Chosen
    Ticket Can Be Picked
    And Seat Can Be Confirmed
