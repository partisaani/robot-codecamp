*** Settings ***
Documentation     A test suite with a single test for checking every movies properties.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file. It makes sure that all presented
...               movies have their properties set.

Resource          finnkino-resource.robot
Test Teardown     Close Browser

*** Test Cases ***
Check Movies' Properties
    Given Browser Is Opened To Finnkino Main Page
    And Movies Page Is Open
    Then Synopses Should Not Be Empty
    And Titles Should Not Be Empty